<?php


require_once('../../config.php');
require_once('lib.php');

global $CFG;

require_once($CFG->dirroot . '/mod/quiz/locallib.php');
require_once('../../mod/quiz/attemptlib.php');

require_login();

$attempt_id = optional_param('attempt_id', 0, PARAM_INT);
if(!$attempt_id)
	die();

$attemptobj = quiz_attempt::create($attempt_id);
$slots = $attemptobj->get_slots();

?>

<?= html_writer::start_tag('ul') ?>
	<?php foreach ($slots as $slot):?>
		<?php if ($attemptobj->is_question_flagged($slot)): ?>
			<?php echo html_writer::tag('li',  $attemptobj->get_question_name($slot) ); ?>
		<?php endif; ?>
	<?php endforeach; ?>
<?= html_writer::end_tag('ul') ?>

<?php exit(); ?>

