<?php

// This file is part of the ecampus module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * TalentQuest version file.
 *
 * @package    local_notifications
 * @author     TalentQuest
 * @copyright  2016 talentquest.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require_once("../../config.php");
$contextid = SYSCONTEXTID;

require_once("lib.php");
notifications_getmy_courses(1);

require_login();

$id = optional_param('id', 0, PARAM_INT);
$status = optional_param('status', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);

$notifications = $DB->get_records_sql("SELECT n.*, nd.alert, nd.email, nd.mobile, nd.status as dstatus 
                                            FROM {local_notifications} n 
                                        LEFT JOIN {local_nots_userdata} nd ON nd.notid = n.id AND nd.userid = $USER->id WHERE n.status = 1 ORDER BY sortorder");

$title = "Notification Center";

$PAGE->set_context(context_system::instance());
$PAGE->set_url(new moodle_url("/local/notifications/index.php", array()));
$PAGE->requires->jquery();
$PAGE->navbar->add($title, new moodle_url('/mod/ecampus/notifications.php'));
$PAGE->set_title($title);
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);

echo $OUTPUT->header();

?>

<div class="notifications-settings">
	<?php echo $OUTPUT->heading($title); ?>
	
    <div class="users-tabs">	
		<table class="notifications-table">
			<tr>
				<th></th>
				<th class="center">On/Off</th>
				<th class="center">Alert</th>
				<th class="center">Email</th>
				<th class="center">Mobile</th>
				<th class="center">Settings</th>
			</tr>
			<?php $category = ''; ?>
			<?php foreach($notifications as $item): ?>
				<?php if ($category != $item->category) : ?>
					<tr>
						<td class="not-category"><strong><?php echo $item->category; ?></strong></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				<?php endif; ?>
				<tr id="not_item_<?php echo $item->id; ?>" not-id="<?php echo $item->id; ?>" class="<?php echo (!isset($item->dstatus) or (isset($item->dstatus) and $item->dstatus == 0)) ? 'disabled' : ''?>">
					<td><?php echo $item->subject; ?></td>
					<td class="actions"><span class="btn btn-xs trigger <?php echo ($item->dstatus == 1) ? 'btn-success' : ''; ?>"><?php echo ($item->dstatus == 1) ? 'on' : 'off'; ?></span></td>
					<td class="actions"><span class="actions-trigger" a-type="alert"><i class="fa <?php echo ($item->alert == 1) ? 'fa-check-square-o' : 'fa-square-o'; ?>"></i></span></td>
					<td class="actions"><span class="actions-trigger" a-type="email"><i class="fa <?php echo ($item->email == 1) ? 'fa-check-square-o' : 'fa-square-o'; ?>"></i></span></td>
					<td class="actions"><span class="actions-trigger" a-type="mobile"><i class="fa <?php echo ($item->mobile == 1) ? 'fa-check-square-o' : 'fa-square-o'; ?>"></i></span></td>
					<td class="actions">
						<?php if ($item->courses_selection > 0) : ?>
							<span class="course-selector"><i class="fa fa-cog"></i></span>
						<?php endif; ?>
					</td>
				</tr>						
				<tr id="not_item_formbox_<?php echo $item->id; ?>" not-id="<?php echo $item->id; ?>" class="not-formbox">
					<td colspan="9"><div class="not-form-box"></div></td>
				</tr>
				<?php $category = $item->category; ?>
			<?php endforeach; ?>
		</table><br />
	</div>
</div>
<script>
	jQuery(document).ready(function(){
		jQuery('.notifications-settings .notifications-table .trigger').click(function(){
			jQuery(this).toggleClass('btn-success');
			jQuery(this).parent().parent().toggleClass('disabled');
			var id = jQuery(this).parent().parent().attr('not-id');
			if(jQuery(this).hasClass('btn-success')){
				jQuery(this).html('on');
				var state = 1;
			}else{
				jQuery(this).html('off');
				var state = 0;
			}
			var button = jQuery(this);
			var button_text = jQuery(this).html();
			jQuery.ajax({
				url: "<?php echo $CFG->wwwroot; ?>/local/notifications/ajax.php",
				type: "POST",
				data: 'action=not-set-status&id='+id+'&status='+state,
				dataType: "json",
				beforeSend: function(){
					jQuery(button).html('<i class="fa fa-spinner fa-spin"></i>');
				}
			}).done(function( data ) {
				jQuery(button).html(button_text);
			});
		});
		
		jQuery('.notifications-settings .notifications-table .actions-trigger').click(function(){
			if (!jQuery(this).parent().parent().hasClass('disabled')){
				var id = jQuery(this).parent().parent().attr('not-id');
				var atype = jQuery(this).attr('a-type');
				if(jQuery(this).find('.fa').hasClass('fa-check-square-o')){
					var state = 0;
					button_text = '<i class="fa fa-square-o"></i>';
				}else{
					var state = 1;
					button_text = '<i class="fa fa-check-square-o"></i>';
				}
				var button = jQuery(this);
				jQuery.ajax({
					url: "<?php echo $CFG->wwwroot; ?>/local/notifications/ajax.php",
					type: "POST",
					data: 'action=not-set-action&id='+id+'&status='+state+'&atype='+atype,
					dataType: "json",
					beforeSend: function(){
						jQuery(button).html('<i class="fa fa-spinner fa-spin"></i>');
					}
				}).done(function( data ) {
					jQuery(button).html(button_text);
				});
			}
		});
		
		jQuery('.notifications-settings .notifications-table .course-selector').click(function(){
			if (!jQuery(this).parent().parent().hasClass('disabled')){
				var id = jQuery(this).parent().parent().attr('not-id');
				jQuery('#not_item_formbox_'+id).toggleClass('active');
				jQuery('#not_item_formbox_'+id+' .not-form-box').slideToggle();
				jQuery('#not_item_formbox_'+id+' .not-form-box').toggleClass('active');
				if (jQuery('#not_item_formbox_'+id+' .not-form-box').hasClass('active')){
					jQuery('#not_item_formbox_'+id+' .not-form-box').html('<i class="fa fa-spin fa-spinner"></i>');
					jQuery('#not_item_formbox_'+id+' .not-form-box').load("<?php echo $CFG->wwwroot;?>/local/notifications/ajax.php?action=not-load-formdata&id="+id, function(){
						jQuery('#not_item_formbox_'+id+' .not-form-box .fa-spin').remove();
					});
				} else {
					jQuery('#not_item_formbox_'+id+' .not-form-box').empty();
				}
			}
		});
	});
</script>

<?php
echo $OUTPUT->footer();