<?php
/**
 * ELIS(TM): Enterprise Learning Intelligence Suite
 * Copyright (C) 2008-2013 Remote-Learner.net Inc (http://www.remote-learner.net)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Mxschool
 *
 * @package    local_mxschool
 * @author     Middlesex School
 * @copyright  2016 mxschool.edu
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_notifications_install()
{
    global $DB;

    $notifications = array();
    $notifications[] = [
        'type' => 'course',
        'courses_selection' => 0,
        'roles_settings' => 0,
        'name' => 'User enrolled in course',
        'description' => 'User enrolled in course',
        'tags' => 'a:7:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"system name";i:5;s:11:"course name";i:6;s:21:"course name with link";}',
        'subject' => 'User enrolled in course',
        'body' => 'Text goes here',
        'sender' => 'supportuser',
        'copy' => '',
        'status' => 1,
        'roles' => '',
        'category' => 'Course Notifications',
        'sortorder' => 1,
    ];

    $notifications[] = [
        'type' => 'course',
        'courses_selection' => 0,
        'roles_settings' => 0,
        'name' => 'User enroll updated in course',
        'description' => 'User enroll updated in course',
        'tags' => 'a:7:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"system name";i:5;s:11:"course name";i:6;s:21:"course name with link";}',
        'subject' => 'User enroll updated in course',
        'body' => 'Text goes here',
        'sender' => 'supportuser',
        'copy' => '',
        'status' => 1,
        'roles' => '',
        'category' => 'Course Notifications',
        'sortorder' => 2,
    ];

    $notifications[] = [
        'type' => 'course',
        'courses_selection' => 0,
        'roles_settings' => 0,
        'name' => 'User unenrolled from course',
        'description' => 'User unenrolled from course',
        'tags' => 'a:7:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"system name";i:5;s:11:"course name";i:6;s:21:"course name with link";}',
        'subject' => 'User unenrolled from course',
        'body' => 'Hello [recipient name],<br>You unenrolled from [course name]<br><br>[system name]<br>',
        'sender' => 'supportuser',
        'copy' => '',
        'status' => 1,
        'roles' => '',
        'category' => 'Course Notifications',
        'sortorder' => 3,
    ];

    $notifications[] = [
        'type' => 'course',
        'courses_selection' => 1,
        'roles_settings' => 1,
        'name' => 'Module added in course',
        'description' => 'Module added in course',
        'tags' => 'a:10:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"system name";i:5;s:11:"course name";i:6;s:21:"course name with link";i:7;s:12:"module title";i:8;s:22:"module title with link";i:9;s:11:"module name";}',
        'subject' => 'Module added in course',
        'body' => 'Module added in course',
        'sender' => 'supportuser',
        'copy' => '',
        'status' => 1,
        'roles' => 'a:5:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";i:3;s:1:"4";i:4;s:1:"5";}',
        'category' => 'Modules Notifications',
        'sortorder' => 4,
    ];

    $notifications[] = [
        'type' => 'course',
        'courses_selection' => 1,
        'roles_settings' => 1,
        'name' => 'Module edited in course',
        'description' => 'Module edited in course',
        'tags' => 'a:10:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"system name";i:5;s:11:"course name";i:6;s:21:"course name with link";i:7;s:12:"module title";i:8;s:22:"module title with link";i:9;s:11:"module name";}',
        'subject' => 'Module edited in course',
        'body' => 'Module edited in course',
        'sender' => 'supportuser',
        'copy' => '',
        'status' => 1,
        'roles' => 'a:5:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";i:3;s:1:"4";i:4;s:1:"5";}',
        'category' => 'Modules Notifications',
        'sortorder' => 5,
    ];

    $notifications[] = [
        'type' => 'course',
        'courses_selection' => 0,
        'roles_settings' => 0,
        'name' => 'Module deleted from course',
        'description' => 'Module deleted from course',
        'tags' => 'a:10:{i:0;s:14:"recipient name";i:1;s:15:"recipient email";i:2;s:17:"support user name";i:3;s:13:"support email";i:4;s:11:"system name";i:5;s:11:"course name";i:6;s:21:"course name with link";i:7;s:12:"module title";i:8;s:22:"module title with link";i:9;s:11:"module name";}',
        'subject' => 'Module deleted from course',
        'body' => 'Module deleted from course',
        'sender' => 'supportuser',
        'copy' => '',
        'status' => 1,
        'roles' => 'a:5:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";i:3;s:1:"4";i:4;s:1:"5";}',
        'category' => 'Modules Notifications',
        'sortorder' => 6,
    ];


    foreach ($notifications as $notification) {
        $DB->insert_record('local_notifications', $notification, false);
    }
}