<?php

require_once('../../config.php');
require_once('lib.php');

require_login();

$alert_id = optional_param('alert_id', 0, PARAM_INT);

global $DB, $USER;

$record = $DB->get_record('local_nots_alerts', ['id' => $alert_id, 'userid' => $USER->id]);
$record->new = false;
$DB->update_record('local_nots_alerts', $record);

echo json_encode(['code' => 200]);

