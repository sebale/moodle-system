<?php

class sbNotifications {
    protected $_id;
    protected $_params;
    
    /**
     * Form definition.
     */
    function __construct($id = 0, $params = array()) {
        $this->_id = $id;
        $this->_params = $params;
    }
    
    function rebuild($id = 0, $params = array()) {
        $this->_id = $id;
        $this->_params = $params;
    }

    function process() {
        global $DB;

        $params = (object)$this->_params;
        
        if ($this->_id) {
            $id = $this->_id;
            $notification = $DB->get_record('local_notifications', array('id'=>$id));
            $site = get_site();
            $supportuser = core_user::get_support_user();
            $message = new stdClass();
            $log = new stdClass();
            $log->notid = $id;
            $item = (isset($params->item)) ? $params->item : [];
            $item['support user name'] = $supportuser->firstname.' '.$supportuser->lastname;
            $item['support email'] = $supportuser->email;
            $item['system name'] = $site->fullname;
            $message->params = $item;
            $notification->roles = ($notification->roles != '') ? unserialize($notification->roles) : array();
            
            if ($notification->status > 0 && isset($item['userid'])) {
                    
                if ($item['n_alert'] == 1) {
                    $message->userid = $item['userid'];
                    $message->title = $this->generate_text($notification->subject, $message->params, $notification->tags);
                    $message->body = $this->generate_text($notification->body, $message->params, $notification->tags);
                    $message->notid = $notification->id;
                    $message->courseid = (isset($item['courseid'])) ? $item['courseid'] : 0;
                    $this->alert_to_user($message);

                    $log->alert = 1;
                }

                if (filter_var($item['recipient email'], FILTER_VALIDATE_EMAIL) and $item['n_email'] == 1) {
                    $message->to = $DB->get_record('user', array('id'=>$item['userid']));
                    $message->from = $supportuser;
                    $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                    $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                    email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);

                    $log->email = 1;
                }

                $log->userid = $item['userid'];
                $log->courseid = (isset($item['courseid'])) ? $item['courseid'] : 0;
            }
        }

        return true;
    }
    
    function generate_text($text = '', $params = array(), $tags){
        $message = '';
        if ($text == '') return $message;
        $tags = unserialize($tags);
        if (count($tags) > 0){
            $message = $text;
            foreach ($tags as $tag){
                if (strripos($message, '['.$tag.']') !== FALSE and isset($params[$tag])){
                    $message = str_replace('['.$tag.']', $params[$tag], $message);
                }
            }
        }
        return $message;
    }
    
    function alert_to_user($alert = null){
        global $DB;

        if ($alert){
            $alert->new = 1;
            $alert->timecreated = time();
            $DB->insert_record('local_nots_alerts', $alert);
        }
    }

    function mobilemsg_to_user($msg = null){
        global $DB;

        $msg->carrier = $this->get_carriers($msg->carrier);
        
        if (!empty($msg->carrier)){
            $mail = get_mailer();
            $supportuser = core_user::get_support_user();

            $mail->Sender   = $msg->params['support email'];
            $mail->From     = $msg->params['support email'];
            $mail->FromName = $msg->params['support user name'];

            $mail->IsHTML(false);
            $mail->Subject  = $msg->subject;
            $mail->Body     = strip_tags($msg->body);

            $msg->phone = str_replace('-', '', $msg->phone);
            $msg->phone = str_replace(' ', '', $msg->phone);

            $mail->addAddress($msg->phone.$msg->carrier, '');

            $mail->send();
        }
    }
    
    function get_carriers($carrier = ''){
        $carriers = array(
                'Alltel' => '@message.alltel.com',
                'AT&T' => '@txt.att.net',
                'Boost' => '@myboostmobile.com',
                'C Spire' => '@cspire1.com',
                'CellularOne' => '@mobile.celloneusa.com',
                'Cingular' => '@cingularme.com',
                'Cricket' => '@sms.mycricket.com',
                'Nextel' => '@messaging.nextel.com',
                'Sprint PCS' => '@messaging.sprintpcs.com',
                'T-Mobile' => '@tmomail.net',
                'US Cellular' => '@email.uscc.net',
                'Verizon' => '@vtext.com',
                'Virgin Mobile' => '@vmobl.com'
            );
        
        if ($carrier == ''){
            return $carriers;
        }
        if (isset($carriers[$carrier])){
            return $carriers[$carrier];
        } else {
            return '';
        }
    }
}


function notificatons_get_tags(){
    $tags = array(
        'recipient name'=>'Recipient name', 
        'recipient email'=>'Recipient Email', 
        'support user name'=>'Support User name',
        'support email'=>'Support Email', 
        'system name'=>'System name', 
        'user name'=>'User fullname', 
        'email'=>'User Email', 
        'course name'=>'Course name', 
        'course name with link'=>'Course name as link', 
        'module title'=>'Module title', 
        'module title with link'=>'Module title as link', 
        'module name'=>'Activity or Resource module name', 
        'content name'=>'Course content name', 
        'due date'=>'Due Date', 
        'enrollment expiration date'=>'Enrollment expiration date', 
        'course end date'=>'Course end date', 
        'student name'=>'Student name', 
        'student email'=>'Student Email', 
        'teacher name'=>'Teacher name', 
        'teacher email'=>'Teacher Email', 
        'event'=>'Event text', 
    );
    return $tags;
}

function notifications_getmy_courses($id){
	global $DB, $USER; 
	$result = array();
	$notification = $DB->get_record('local_notifications', array('id'=>$id));
    $notification->roles = unserialize($notification->roles);
		
	$mycourses = enrol_get_my_courses('', 'fullname ASC');
	$courses = array();
    if (count($notification->roles)){
        foreach ($mycourses as $item){
            $ccontext = context_course::instance($item->id);
            $roles = get_user_roles($ccontext, $USER->id);
                
            foreach ($roles as $role) {
                if (in_array($role->roleid, $notification->roles)){
                    $courses[$item->id] = $item;
                }
            }
        }    
    }
	
	foreach ($courses as $course){
		$result[$course->id] = $course;
	}
	return $result;
}
/*
function notifications_get_courses($id){
	global $DB, $USER;

	$courses = $DB->get_records_sql("SELECT c.* FROM {local_nots_data} nd LEFT JOIN {course} c ON c.id = nd.data_id WHERE nd.not_id = $id AND nd.data_type = 'course' AND nd.state = 1 AND nd.userid = $USER->id");

	$result = array();
	foreach ($courses as $course){
		$result[$course->id] = $course->id;
	}
	return $result;
}

function local_notifications_cron(){
    global $DB, $USER;
}*/

?>

