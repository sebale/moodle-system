<?php

require_once('../../config.php');
require_once('lib.php');

require_login();

$id = optional_param('course_id', 0, PARAM_INT);
$sections = get_course_group($id);

?>

<?= html_writer::start_tag('ul', ['class' => 'group']) ?>
	<?php foreach($sections['root'] as $key => $section): ?>
		<?= html_writer::tag('li',
			html_writer::tag('p', html_writer::tag('span', '+') . get_section_name($id, $section), [
				'class' => 'group-item',
				'data-id' => $section->id
			])
		); ?>
	<?php endforeach; ?>
<?= html_writer::end_tag('ul') ?>

<?php exit(); ?>

