<?php

require_once('../../config.php');
require_once('lib.php');

require_login();

$id = optional_param('course_id', 0, PARAM_INT);
$sections = get_course_group($id);

$show_curse_id = 0;
?>

<?= html_writer::start_tag('ul', ['class' => 'group']) ?>
	<?php foreach($sections['root'] as $key => $section): $show_curse_id++; ?>
		<?= html_writer::tag('li',
			html_writer::tag('p', html_writer::tag('span', '+') . ' ' . (($show_curse_id < 10) ? ('0' . $show_curse_id) : $show_curse_id) . ' ' . get_section_name($id, $section['obj']), [
				'class' => 'group-item section-status ' . $section['class'],
				'data-id' => $section['obj']->id
			])
		); ?>
	<?php endforeach; ?>
<?= html_writer::end_tag('ul') ?>

<?php exit(); ?>

