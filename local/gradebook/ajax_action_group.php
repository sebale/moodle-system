<?php

require_once('../../config.php');
require_once('lib.php');

require_login();


$id = optional_param('course_id', 0, PARAM_INT);
$group_id = optional_param('group_id', 0, PARAM_INT);

$groups = get_course_sub_group($id, $group_id);

?>

<?php if($groups): ?>
	<?= html_writer::start_tag('ul', ['class' => 'categories']) ?>
	<?php foreach($groups as $key => $group): ?>
		<?= html_writer::start_tag('li'); ?>
			<?= html_writer::tag('p', html_writer::tag('span', '+') . get_section_name($id, $group), [
					'class' => 'category-item',
					'data-id' => $group->id
				])
			?>

			<?= html_writer::start_tag('ul', ['class' => 'sections']) ?>
				<?php foreach(get_course_sub_group($id, $group->id) as $section): ?>
					<?= html_writer::start_tag('li'); ?>
						<?= html_writer::tag('p', html_writer::tag('span', '+') . get_section_name($id, $section), [
								'class' => 'section-item',
								'data-id' => $section->id
							]); ?>

						<?php if($quiz_list = get_quiz_list($id, $section->id)): ?>
							<table>
								<thead>
								<colgroup>
									<col width="25%">
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
								</colgroup>
								<tr>
									<th>Quiz Name</th>
									<th>Progress</th>
									<th>Started</th>
									<th>Completed</th>
									<th>Time Taken</th>
									<th>Attempts</th>
									<th>AVG. Grade</th>
								</tr>
								</thead>
								<tbody>
								<?php foreach($quiz_list as $quiz): ?>
									<tr>
										<td><p><?= $quiz['name'] ?></p></td>
										<td><?= $quiz['state'] ?></td>
										<td><?= $quiz['time_start'] ? date('d M H:i:s', $quiz['time_start']) : '-' ?></td>
										<td><?= $quiz['time_finish'] ? date('d M H:i:s', $quiz['time_finish']) : '-' ?></td>
										<td><?= $quiz['time_taken'] ? date('H:i:s', $quiz['time_taken']) : '-' ?></td>
										<td><p><?= $quiz['attempt'] ?></p></td>
										<td><?= $quiz['avg_grade'] ?><!--A/60.5%--></td>
									</tr>
								<?php endforeach;?>
								</tbody>
							</table>
						<?php endif; ?>
					<?= html_writer::end_tag('li'); ?>
				<?php endforeach; ?>
			<?= html_writer::end_tag('ul') ?>
		<?= html_writer::end_tag('li'); ?>
	<?php endforeach; ?>
	<?= html_writer::end_tag('ul') ?>
<?php endif; ?>

<?php exit(); ?>

