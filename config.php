<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

error_reporting(E_ALL);ini_set('display_errors', '1');$CFG->debug = 32767;  $CFG->debugdisplay = true;

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'institutes';
$CFG->dbuser    = 'institutes';
$CFG->dbpass    = 'Moodle_2016';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
);

$CFG->wwwroot   = 'http://institutes.sebale.net';
$CFG->dataroot  = '/home/institutes/public_html/moodledata';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

require_once(dirname(__FILE__) . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
